-- | An infinitely long list of numbers, starting at index 1, with each number being the count of factors for that index
compositeCounts = map countFactors [1..]

findFactors :: Int -> [Int]
findFactors x = [n | n <- [1..x], x `mod` n == 0]

countFactors :: Int -> Int
countFactors = length . findFactors

-- |
-- | Find a pair, with first being the number before x with the most factors, the second being those factors
-- |
findMostComposite :: Int -> (Int, [Int])
findMostComposite limit = foldr (\x acc -> if (countFactors x) > (length (snd acc)) then (x, findFactors x) else acc) (0, [0]) [1..limit]

